import type Product from "@/types/Product";
import http from "./axios";
function getProducts() {
  return http.get("/products");
}
function saveProducts(product: Product) {
  return http.post("/products", product);
}
function updateProducts(id: number, product: Product) {
  return http.patch(`/products/${id}`, product);
}
function deleteProducts(id: number) {
  return http.patch(`/products/${id}`);
}
export default { getProducts, saveProducts, updateProducts, deleteProducts };
